---
document: modulemd
version: 2
data:
  name: parfait
  stream: 0.5
  summary: Parfait Module
  description: >-
    Parfait is a Java performance monitoring library that exposes and collects metrics
    through a variety of outputs.  It provides APIs for extracting performance metrics
    from the JVM and other sources. It interfaces to Performance Co-Pilot (PCP) using
    the Memory Mapped Value (MMV) machinery for extremely lightweight instrumentation.
  license:
    module:
    - ASL 2.0
  dependencies:
  - buildrequires:
      javapackages-tools: [201801]
      pki-deps: [10.6]
      platform: [el8]
    requires:
      javapackages-runtime: [201801]
      pki-deps: [10.6]
      platform: [el8]
  profiles:
    common:
      rpms:
      - parfait
      - parfait-examples
      - pcp-parfait-agent
  api:
    rpms:
    - parfait
    - parfait-examples
    - pcp-parfait-agent
  components:
    rpms:
      parfait:
        rationale: Main Parfait Package
        ref: stream-0.5-rhel-8.5.0
        buildorder: 70
      si-units:
        rationale: A library of SI quantities and unit types (JSR 363).
        ref: stream-0.5-rhel-8.5.0
        buildorder: 50
      unit-api:
        rationale: "The Unit of Measurement library provides a set of Java language
          programming interfaces for handling units and quantities. The interfaces
          provide a layer which separates client code, which would call the API, from
          library code, which implements the API.\nThe specification contains Interfaces
          and abstract classes with methods for unit operations:\n\n  * Checking of
          unit compatibility\n  * Expression of a quantity in various units\n  * Arithmetic
          operations on units"
        ref: stream-0.5-rhel-8.5.0
        buildorder: 10
      uom-lib:
        rationale: Units of Measurement Libraries - extending and complementing JSR
          363.
        ref: stream-0.5-rhel-8.5.0
        buildorder: 30
      uom-parent:
        rationale: Main parent POM for all Units of Measurement Maven projects.
        ref: stream-0.5-rhel-8.5.0
        buildorder: 20
      uom-se:
        rationale: This package contains documentation for the Units Standard (JSR
          363) Java SE 8 Implementation.
        ref: stream-0.5-rhel-8.5.0
        buildorder: 40
      uom-systems:
        rationale: Units of Measurement Systems - modules for JSR 363.
        ref: stream-0.5-rhel-8.5.0
        buildorder: 60
...
